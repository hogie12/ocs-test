import * as types from "../types";

const initialState = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  message: [],
  post:[],
};

const allPost = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.GET_ALL_POST_PENDING:
      return {
        ...state,
        isLoading: true,
      };
    case types.GET_ALL_POST_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        isLoading: false,
        post:payload
      };
    case types.GET_ALL_POST_FAIL:
      return {
        ...state,
        isError: true,
        isLoading: false,
        message: payload,
      };
    default:
      return state;
  }
};

export default allPost;
