import React from 'react'
import { Outlet } from 'react-router'
import DashboardHeader from '../../component/dashboardHeader'

export default function Dashboard() {
  
  return (
    <div>
      <DashboardHeader/>
      <Outlet/>
    </div>
  )
}
