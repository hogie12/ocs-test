import * as types from "../types";

const initialState = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  message: [],
  comment:[],
};

const allComment = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.GET_ALL_COMMENT_PENDING:
      return {
        ...state,
        isLoading: true,
      };
    case types.GET_ALL_COMMENT_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        isLoading: false,
        comment:payload
      };
    case types.GET_ALL_COMMENT_FAIL:
      return {
        ...state,
        isError: true,
        isLoading: false,
        message: payload,
      };
    default:
      return state;
  }
};

export default allComment;
