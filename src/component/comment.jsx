import React from "react";
import { List, ListItem, ListItemText, ListItemAvatar } from "@mui/material";

export default function CommentCard({comment}) {
  return (
    <div>
      <List sx={{ maxWidth: "30rem", color: "gray" }}>
        <ListItem alignItems="flex-start">
          <ListItemAvatar>
            <h3 style={{ marginRight: "20px", color: "black" }}>
              {comment.name}
            </h3>
          </ListItemAvatar>
          <ListItemText
            primary={<p>{comment.body}</p>}
          />
        </ListItem>
      </List>
    </div>
  );
}
