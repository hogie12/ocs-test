import { combineReducers } from "redux";
import allPost from "./post";
import allUser from "./user";
import allComment from "./comment";
import postById from "./postById";
import userById from "./userById";

export default combineReducers({
  allPost,
  allComment,
  allUser,
  postById,
  userById,
});
