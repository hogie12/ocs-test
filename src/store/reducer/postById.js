import * as types from "../types";

const initialState = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  message: [],
  postDetail:[],
};

const postById = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.GET_POST_BY_ID_PENDING:
      return {
        ...state,
        isLoading: true,
      };
    case types.GET_POST_BY_ID_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        isLoading: false,
        postDetail:payload
      };
    case types.GET_POST_BY_ID_FAIL:
      return {
        ...state,
        isError: true,
        isLoading: false,
        message: payload,
      };
    default:
      return state;
  }
};

export default postById;
