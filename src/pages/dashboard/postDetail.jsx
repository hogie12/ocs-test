import React, { useEffect, useState } from "react";
import { List, ListItem, ListItemText, ListItemAvatar } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getPostById, getAllComment } from "../../store/action";
import { useParams } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import { Link } from "react-router-dom";
import CommentCard from "../../component/comment";

export default function PostDetail() {
  // data from reducer
  const { postDetail } = useSelector((state) => state.postById);
  const { user } = useSelector((state) => state.allUser);
  const { comment } = useSelector((state) => state.allComment);

  const dispatch = useDispatch();
  const { id } = useParams();
  const [showComment, setShowComment] = useState(false);

  useEffect(() => {
    dispatch(getPostById(id));
    dispatch(getAllComment(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  console.log(showComment);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <div style={{ maxWidth: "40rem" }}>
        <Link style={{ alignSelf: "start" }} to="/dashboard/post">
          <ArrowBackIcon fontSize="large" />
        </Link>
        <List sx={{ maxWidth: "40rem", color: "gray" }}>
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <h3 style={{ marginRight: "20px", color: "black" }}>
                {user[postDetail.userId - 1]?.name}
              </h3>
            </ListItemAvatar>
            <ListItemText
              primary={<h3>{postDetail.title}</h3>}
              secondary={
                <>
                  <h5>{postDetail.body}</h5>
                  {showComment ? (
                    <>
                      <h3>All Comment</h3>
                      {comment?.map((data) => (
                        <CommentCard comment={data} key={data.id} />
                      ))}
                    </>
                  ) : (
                    <>
                      <div
                        style={{ marginTop: "10px", display: "flex", cursor:"pointer" }}
                        onClick={() => setShowComment(true)}
                      >
                        <ChatBubbleOutlineIcon />
                        <p style={{ margin: "0px 10px", color: "blue" }}>
                          {comment.length}
                        </p>
                      </div>
                    </>
                  )}
                </>
              }
            />
          </ListItem>
        </List>
      </div>
    </div>
  );
}
