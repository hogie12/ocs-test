import React from "react";
import { Button } from "@mui/material";
import { Link } from "react-router-dom";

export default function Header() {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "10px 50px",
      }}
    >
      <div>
        <h2>Cinta Coding</h2>
      </div>
      <div>
        <Link to="login">
          <Button variant="contained">Login</Button>
        </Link>
      </div>
    </div>
  );
}
