import React from "react";
import { Link } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Grid } from "@mui/material";
import {useSelector} from 'react-redux'

export default function ProfilePage() {
  const { userDetail } = useSelector((state) => state.userById);
  console.log(userDetail)
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <div style={{ minWidth: "40rem" }}>
        <Link style={{ alignSelf: "start", }} to="/dashboard/post">
          <ArrowBackIcon fontSize="large" />
        </Link>
        <Grid container spacing={0} sx={{ paddingLeft: "8rem", paddingTop:"2rem" }}>
          <Grid item xs={3} sx={{ color: "gray" }}>
            <h3>Username</h3>
            <h3>Email</h3>
            <h3>Address</h3>
            <h3>Phone</h3>
          </Grid>
          <Grid item xs={3} sx={{ color: "gray" }}>
            <h3>:</h3>
            <h3>:</h3>
            <h3>:</h3>
            <h3>:</h3>
          </Grid>
          <Grid item xs={6}>
            <h3>{userDetail.username}</h3>
            <h3>{userDetail.email}</h3>
            <h3>{userDetail.address.city}</h3>
            <h3>{userDetail.phone}</h3>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
