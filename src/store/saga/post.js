import axios from "axios";
import * as types from "../types";
import { put, takeEvery } from "redux-saga/effects";

function* getAllPost() {
  try {
    const res = yield axios.get(`https://jsonplaceholder.typicode.com/posts`)
    yield put({
      type: types.GET_ALL_POST_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    yield put({
      type: types.GET_ALL_POST_FAIL,
      payload: error.response,
    });
  }
}

function* getPostById({id}) {
  try {
    const res = yield axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
    yield put({
      type: types.GET_POST_BY_ID_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    yield put({
      type: types.GET_POST_BY_ID_FAIL,
      payload: error.response,
    });
  }
}


export function* watchAllPost() {
  yield takeEvery(types.GET_ALL_POST_PENDING, getAllPost);
}

export function* watchPostById() {
  yield takeEvery(types.GET_POST_BY_ID_PENDING, getPostById);
}