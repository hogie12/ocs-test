import React, { useState } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import { Link } from "react-router-dom";

export default function PostList({ user, title, postId }) {
  const [totalComment] = useState();

  // useEffect(() => {
  //   const comment = async () => {
  //     const res = axios.get(
  //       `https://jsonplaceholder.typicode.com/posts/${postId}/comments`
  //     );
  //     const data = res.data;
  //     setTotalComment(data);
  //   };
  //   comment();
  // // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  return (
    <List sx={{ maxWidth: "40rem", color: "gray" }}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <h3 style={{ marginRight: "20px", color: "black" }}>{user?.name}</h3>
        </ListItemAvatar>
        <ListItemText
          primary={
            <>
              <h3 style={{ marginBottom: "0px" }}>{title}</h3>
              <div
                style={{ marginTop: "10px", display: "flex", color: "blue" }}
              >
                <ChatBubbleOutlineIcon />
                <p style={{ margin: "0px 10px", color: "blue" }}>
                  {totalComment?.length}
                </p>
                <Link
                  to={`/dashboard/post/${postId}`}
                  style={{
                    margin: "0px 10px",
                    color: "blue",
                    textDecoration: "none",
                  }}
                >
                  Detail
                </Link>
              </div>
            </>
          }
        />
      </ListItem>
    </List>
  );
}
