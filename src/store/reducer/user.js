import * as types from "../types";

const initialState = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  message: [],
  user:[],
};

const allUser = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.GET_ALL_USER_PENDING:
      return {
        ...state,
        isLoading: true,
      };
    case types.GET_ALL_USER_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        isLoading: false,
        user:payload
      };
    case types.GET_ALL_USER_FAIL:
      return {
        ...state,
        isError: true,
        isLoading: false,
        message: payload,
      };
    default:
      return state;
  }
};

export default allUser;
