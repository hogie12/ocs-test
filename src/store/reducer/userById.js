import * as types from "../types";

const initialState = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  message: [],
  userDetail:[],
};

const userById = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.GET_USER_BY_ID_PENDING:
      return {
        ...state,
        isLoading: true,
      };
    case types.GET_USER_BY_ID_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        isLoading: false,
        userDetail:payload
      };
    case types.GET_USER_BY_ID_FAIL:
      return {
        ...state,
        isError: true,
        isLoading: false,
        message: payload,
      };
    default:
      return state;
  }
};

export default userById;
