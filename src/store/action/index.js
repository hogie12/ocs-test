import * as types from "../types";

export const getAllPost = () => {
  return {
    type: types.GET_ALL_POST_PENDING,
  };
};

export const getAllComment = (id) => {
  return {
    type: types.GET_ALL_COMMENT_PENDING,
    id
  };
};

export const getAllUser = () => {
  return {
    type: types.GET_ALL_USER_PENDING,
  };
};

export const getPostById = (id) => {
  return {
    type: types.GET_POST_BY_ID_PENDING,
    id
  };
};

export const getUserById = (id) => {
  return {
    type: types.GET_USER_BY_ID_PENDING,
    id
  };
};
