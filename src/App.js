import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Homepage from "./pages/homepage";
import LoginPage from "./pages/login";
import Dashboard from "./pages/dashboard/dashboard";
import Post from "./pages/dashboard/postList";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getAllPost, getAllUser } from "./store/action";
import PostDetail from "./pages/dashboard/postDetail";
import ProfilePage from "./pages/dashboard/profile";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getAllPost());
    dispatch(getAllUser());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Router>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="login" element={<LoginPage />} />
        <Route path="dashboard" element={<Dashboard />}>
          <Route path="post" element={<Post />} />
          <Route path="post/:id" element={<PostDetail />} />
          <Route path="profile" element={<ProfilePage />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
