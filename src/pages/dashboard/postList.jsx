import React, { useState } from "react";
import { useSelector } from "react-redux";
import PostList from "../../component/list";
import {
  Pagination,
  OutlinedInput,
  InputAdornment,
  FormControl,
  InputLabel,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

export default function Post() {
  const { post } = useSelector((state) => state.allPost);
  const { user } = useSelector((state) => state.allUser);
  const [currentPage, setCurrentPage] = useState(1);
  const [postPerPage] = useState(3);

  const indexOfLastPost = currentPage * postPerPage;
  const indexOfFirstPost = indexOfLastPost - postPerPage;
  const currentPosts = post.slice(indexOfFirstPost, indexOfLastPost);

  const handleChangePage = (event, value) => {
    setCurrentPage(value);
  };
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <div>
        <FormControl sx={{ m: 1, width: "40rem" }} variant="outlined">
          <InputLabel htmlFor="outlined-search">Search</InputLabel>
          <OutlinedInput
            id="outlined-search"
            endAdornment={
              <InputAdornment position="end">
                <SearchIcon />
              </InputAdornment>
            }
            label="search"
          />
        </FormControl>
      </div>
      <div style={{ minHeight: "420px" }}>
        {currentPosts?.map((data) => (
          <PostList
            key={data.id}
            user={user[data.userId - 1]}
            title={data.title}
            postId={data.id}
          />
        ))}
      </div>
      <Pagination
        count={Math.ceil(post.length / postPerPage)}
        page={currentPage}
        onChange={handleChangePage}
      />
    </div>
  );
}
