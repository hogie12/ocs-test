import axios from "axios";
import * as types from "../types";
import { put, takeEvery } from "redux-saga/effects";

function* getAllComment({id}) {
  try {
    const res = yield axios.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
    yield put({
      type: types.GET_ALL_COMMENT_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    yield put({
      type: types.GET_ALL_COMMENT_FAIL,
      payload: error.response,
    });
  }
}


export function* watchAllComment() {
  yield takeEvery(types.GET_ALL_COMMENT_PENDING, getAllComment);
}