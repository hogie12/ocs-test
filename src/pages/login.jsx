import React, { useState } from "react";
import { Button, TextField } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import {getUserById} from '../store/action/index'
import { Navigate } from "react-router-dom";

export default function LoginPage() {
  // user data from reducer
  const user = useSelector((state) => state.allUser);
  const userDetail = useSelector((state) => state.userById);
  const dispatch = useDispatch()
  
  //state
  const [id, setId] = useState()
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loginSuccess, setLoginSuccess] = useState(true);

  // handle change
  const handleChangeUsername = (event) => {
    setUsername(event.target.value);
  };
  const handleChangePassword = (event) => {
    setPassword(event.target.value);
  };

  //validation
  const isUser = () => {
    // eslint-disable-next-line no-unused-vars
    const userId = user.user.findIndex((object) => {
      if(object.username === username){
        return setId(object.id)
      } else{
        return false
      }
    });
    return user.user.some(function (e) {
      return e.username === username && e.username === password;
    });
  };

  // get user detail by id
  const handleLogin = (e) =>{
    e.preventDefault()
    dispatch(getUserById(id))
  }

  // redirect to dashboard
    if(userDetail.isSuccess){
      return (<Navigate to="/dashboard/post" />)
    }

  return (
    <div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          minHeight: "100vh",
          justifyContent: "center",
        }}
      >
        <div>
          <h1 style={{ marginBottom: "50px" }}>Login Page</h1>
        </div>
        <form style={{ display: "flex", flexDirection: "column" }} onSubmit={handleLogin}>
          <TextField
            label="Username"
            variant="outlined"
            color="primary"
            value={username}
            onChange={handleChangeUsername}
            style={{ margin: "20px 0" }}
            error={!loginSuccess}
          />
          <TextField
            label="Password"
            variant="outlined"
            type="password"
            value={password}
            onChange={handleChangePassword}
            error={!loginSuccess}
          />
          <Button
            variant="contained"
            style={{ marginTop: "50px" }}
            onClick={() => setLoginSuccess(isUser())}
            type="submit"
          >
            Login
          </Button>
        </form>
      </div>
    </div>
  );
}
