import axios from "axios";
import * as types from "../types";
import { put, takeEvery } from "redux-saga/effects";

function* getAllUser() {
  try {
    const res = yield axios.get(`https://jsonplaceholder.typicode.com/users`)
    yield put({
      type: types.GET_ALL_USER_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    yield put({
      type: types.GET_ALL_USER_FAIL,
      payload: error.response,
    });
  }
}

function* getUserById({id}) {
  try {
    const res = yield axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
    yield put({
      type: types.GET_USER_BY_ID_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    yield put({
      type: types.GET_USER_BY_ID_FAIL,
      payload: error.response,
    });
  }
}


export function* watchAllUser() {
  yield takeEvery(types.GET_ALL_USER_PENDING, getAllUser);
}
export function* watchUserById() {
  yield takeEvery(types.GET_USER_BY_ID_PENDING, getUserById);
}