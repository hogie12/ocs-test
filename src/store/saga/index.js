import { all } from "@redux-saga/core/effects";
import { watchAllComment } from "./comment";
import { watchAllPost, watchPostById } from "./post";
import { watchAllUser, watchUserById } from "./user";

export default function* rootSaga() {
  yield all([watchAllPost(), watchPostById(), watchAllUser(), watchAllComment(), watchUserById()]);
}
