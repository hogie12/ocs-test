import React, { useState } from "react";
import { Tabs, Tab, Menu, MenuItem } from "@mui/material";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

export default function DashboardHeader() {
  const { userDetail } = useSelector((state) => state.userById);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        padding: "10px 60px",
      }}
    >
      <div>
        <h2>Cinta Coding</h2>
      </div>
      <div>
        <Tabs value={0}>
          <Tab label="Post" />
        </Tabs>
      </div>
      <div>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <Link to={"/dashboard/profile"} style={{color:"black", textDecoration:"none"}}>
            <MenuItem onClick={handleClose}>Detail Profile</MenuItem>
          </Link>
        </Menu>
        <h2 onClick={handleClick} style={{ cursor: "pointer" }}>
          Welcome, <span style={{ color: "blue" }}>{userDetail.username}</span>
        </h2>
      </div>
    </div>
  );
}
